import React, { Component } from 'react'
import './App.css'
import { postsRef } from './firebase'

import { Row } from './components'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      posts: {}
    }

    this._fetch_and_update_posts_stats = this._fetch_and_update_posts_stats.bind(this)
  }
  componentDidMount() {
    // Start listening for new records and changes.
    this._fetch_and_update_posts_stats()
  }
  _handle_added_changed(snapshot) {
    const postId = snapshot.key
    const stats = snapshot.val()
    console.log(stats);
    this.setState({
      posts: {
        ...this.state.posts,
        [postId]: stats
      }
    })
  }
  _fetch_and_update_posts_stats() {
    // New record added
    postsRef.on('child_changed', this._handle_added_changed.bind(this))

    // Any old records and changes to them.
    postsRef.on('child_added', this._handle_added_changed.bind(this))
  }
  
  _renderRows(posts) {
    const postIds = Object.keys(posts)
    return (postIds.length) ? postIds.map((postId) => <Row key={postId} {...posts[postId]} />) 
      : <div className="loading">Loading...</div>
  }

  render() {
    const posts = this.state.posts
    return (
      <div className="App">
        <h1>Stats of Posts</h1>
        <div className="row header">
          <div className="col commented">commented</div>
          <div className="col consumed">consumed</div>
          <div className="col deleted">deleted</div>
          <div className="col received">received</div>
          <div className="col sent">sent</div>
          <div className="col other">EmptyString</div>
        </div>
        {
          this._renderRows(posts)
        }
      </div>
    )
  }
}

export default App;
