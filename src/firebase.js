import * as firebase from "firebase";

var config = {
    apiKey: "AIzaSyApicOR5EJFdvqBIT3ETQE2DvDI6mL2AhM",
    authDomain: "ordev-5e28a.firebaseapp.com",
    databaseURL: "https://ordev-5e28a.firebaseio.com",
    projectId: "ordev-5e28a",
    storageBucket: "ordev-5e28a.appspot.com",
    messagingSenderId: "561015592221"
  };
firebase.initializeApp(config);
export const postsRef = firebase.database().ref("posts");