import React, { Component } from 'react'

import './Row.css'

export class Row extends Component {
    render() {
        const {commented=0, consumed=0, deleted=0, received=0, sent=0, EmptyString=0} = this.props
        return <div className="row">
            <div className="col commented">{commented}</div>
            <div className="col consumed">{consumed}</div>
            <div className="col deleted">{deleted}</div>
            <div className="col received">{received}</div>
            <div className="col sent">{sent}</div>
            <div className="col other">{EmptyString}</div>
        </div>
    }
}